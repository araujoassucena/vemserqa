//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {

        ContaCorrente contaCorrenteCliente1 = new ContaCorrente();

        Contato contatoCliente1 = new Contato();
        contatoCliente1.descricao = "Celular de trabalho";
        contatoCliente1.telefone = "88888888888";
        contatoCliente1.tipo = 1;

        Contato contato2Cliente1 = new Contato();
        contato2Cliente1.descricao = "Celular de casa";
        contato2Cliente1.telefone = "99999999999";
        contato2Cliente1.tipo = 2;

        Endereco enderecoCliente1 = new Endereco();
        enderecoCliente1.tipo = 1;
        enderecoCliente1.logradouro = "Rua Exemplo";
        enderecoCliente1.numero = 123;
        enderecoCliente1.complemento = "Apto 45";
        enderecoCliente1.cep = "12345-678";
        enderecoCliente1.cidade = "Cidade Exemplo";
        enderecoCliente1.estado = "EX";
        enderecoCliente1.pais = "Brasil";

        Endereco endereco2Cliente1 = new Endereco();
        endereco2Cliente1.tipo = 1;
        endereco2Cliente1.logradouro = "Rua asdasd";
        endereco2Cliente1.numero = 12223;
        endereco2Cliente1.complemento = "Aptoasdasd 45";
        endereco2Cliente1.cep = "15212-678";
        endereco2Cliente1.cidade = "Cida1212de Exemplo";
        endereco2Cliente1.estado = "E212X";
        endereco2Cliente1.pais = "Br212asil";

        Cliente cliente1 = new Cliente();
        cliente1.nome = "bella 1";
        cliente1.cpf = "000.000.000-00";
        cliente1.contatos[0] = contatoCliente1;
        cliente1.contatos[1] = contato2Cliente1;
        cliente1.enderecos[0] = enderecoCliente1;
        cliente1.enderecos[1] = endereco2Cliente1;

        cliente1.imprimirCliente();
        cliente1.imprimirContatos(); // chamada para imprimir info
        cliente1.imprimirEnderecos();

        contaCorrenteCliente1.cliente = cliente1;
        contaCorrenteCliente1.numeroConta = "1235";
        contaCorrenteCliente1.agencia = 123;
        contaCorrenteCliente1.saldo = 1000;
        contaCorrenteCliente1.chequeEspecial = 300;


        ContaCorrente contaCorrenteCliente2 = new ContaCorrente();

        Contato contatoCliente2 = new Contato();
        contatoCliente2.descricao = "Celular de trabalho";
        contatoCliente2.telefone = "88888888888";
        contatoCliente2.tipo = 1;

        Endereco enderecoCliente2 = new Endereco();
        enderecoCliente2.tipo = 1;
        enderecoCliente2.logradouro = "Rua Exemplo";
        enderecoCliente2.numero = 123;
        enderecoCliente2.complemento = "Apto 45";
        enderecoCliente2.cep = "12345-678";
        enderecoCliente2.cidade = "Cidade Exemplo";
        enderecoCliente2.estado = "EX";
        enderecoCliente2.pais = "Brasil";

        Cliente cliente2 = new Cliente();
        cliente2.nome = "lulu 2";
        cliente2.cpf = "000.000.000-00";
        cliente2.contatos[0] = contatoCliente2;
        cliente2.enderecos[0] = enderecoCliente2;

        contaCorrenteCliente2.cliente = cliente2;
        contaCorrenteCliente2.numeroConta = "4231";
        contaCorrenteCliente2.agencia = 431;
        contaCorrenteCliente2.saldo = 500;
        contaCorrenteCliente2.chequeEspecial = 50;

        ///////////////////////////////////////////////////////////////////////////////////////
        // tentar sacar mais de 1300 em uma conta com saldo de 1000 e 300 de cheque
        boolean sacou = contaCorrenteCliente1.sacar(1300.0001);
        System.out.println("foi possível sacar 1300.0001 de 1000 com 300 de cheque especial? " + sacou);

        // sacar valor negativo
        boolean sacou2 = contaCorrenteCliente1.sacar(-20);
        System.out.println("foi possível sacar -20? " + sacou2);

        // sacar valor válido
        boolean sacou3 = contaCorrenteCliente1.sacar(300);
        System.out.println("foi possível sacar 300 com 1000 de saldo e 300 de cheque especial? " + sacou3);
        System.out.println("O saldo foi atualizado após sacar 300? " + contaCorrenteCliente1.saldo);

        // sacar valor alto após o saque
        boolean sacou4 = contaCorrenteCliente1.sacar(1300.0001);
        System.out.println("foi possível sacar 1300.0001 após realizar um saque anteriormente? " + sacou4);

        // depositar 0 reais
        boolean depositar = contaCorrenteCliente1.depositar(0);
        System.out.println("foi possível depositar 0? " + depositar);

        // depositar 100
        boolean depositar1 = contaCorrenteCliente1.depositar(100);
        System.out.println("foi possível depositar 0? " + depositar1);
        System.out.println("O saldo foi atualizado após depositar 100? " + contaCorrenteCliente1.saldo);


        System.out.println("total cliente 1 " + contaCorrenteCliente1.saldo);
        System.out.println("total cliente 2 " + contaCorrenteCliente2.saldo);

        // transferir 500 reais para a conta corrente 2 com sucesso
        boolean transferiu = contaCorrenteCliente1.transferir(contaCorrenteCliente2, 500);

        System.out.println("foi possível transferir 500 de cliente 1 para cliente2? " + transferiu);
        System.out.println("total cliente 1 " + contaCorrenteCliente1.saldo);
        System.out.println("total cliente 2 " + contaCorrenteCliente2.saldo);

        // tentar transferir 1500 sem ter dinheiro em saldo
        boolean transferir1 = contaCorrenteCliente1.transferir(contaCorrenteCliente2, 1500);
        System.out.println("foi possível transferir 1500 com 500 de saldo e 300 de cheque especial? " + transferir1);
        System.out.println("total cliente 1 " + contaCorrenteCliente1.saldo);
        System.out.println("total cliente 2 " + contaCorrenteCliente2.saldo);

        contaCorrenteCliente1.imprimirContaCorrente();
        contaCorrenteCliente2.imprimirContaCorrente();
    }

}