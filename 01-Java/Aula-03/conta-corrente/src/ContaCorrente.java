public class ContaCorrente  {
    Cliente cliente;
    String numeroConta;
    int agencia;
    double saldo;
    double chequeEspecial;


    public void imprimirContaCorrente(){

        System.out.println("=================== Conta corrente ===================");
        System.out.println("numero da conta: " +this.numeroConta);
        System.out.println("agencia: " + this.agencia);
        System.out.println("saldo: " + this.saldo);
        System.out.println("cheque especial: " + this.chequeEspecial);
    }

    // Os métodos possuem diversas particularidades e composto por algumas palavras chavess
    // exemplo: public boolean sacar(double valor){}
    // public = visibilidade
    // boolean = retorno
    // sacar = nome do método
    // (double valor) = parâmetros
    public boolean sacar(double valor){
        if(valor <= 0){
            System.out.println("Não é possível sacar valor menor ou igual a 0");
            return false;
        }

        if(valor > this.saldo + this.chequeEspecial){
            System.out.println("Não é possível sacar valor maior que a soma do saldo + saque especial");

            return false;
        }

        this.saldo = this.saldo - valor;
        System.out.println("Foi realizado o saque de: " + valor); //atualizar saldo
        System.out.println("O saldo atual é de: " + this.saldo);

        return true;
    }

    public boolean depositar(double valor){
        if(valor <= 0){
            System.out.println("Não é possível depositar valor menor ou igual a 0");
            return false;
        }
        this.saldo = this.saldo + valor;
        return true;

    }

    public double retornarSaldoComChequeEspecial(){
        return this.saldo + this.chequeEspecial;
    }

    public boolean transferir(ContaCorrente contaCorrente, double valor){ // valor que vem na main

        boolean sacou = this.sacar(valor); //Já verifica se o valor é positivo ou negativo para o deposito na CC
        if(sacou){
            contaCorrente.depositar(valor);
            return true;
        }

        return false;
    }


}
