public class Cliente {

    String nome;
    String cpf;
    Contato[] contatos = new Contato[2];
    Endereco[] enderecos = new Endereco[2];

    public void imprimirContatos(){

        // Contato contato é uma nova varíavel que está sendo criada dentro do for para poder acessar cada posição do array de contatos;
        // Na primeira interação do for a variável contato vai ser igual a contatos[0]
        // Na segunda interação do for a variável contato vai ser igual a contatos[1]
        // Na terceira interação do for a variável contato vai ser igual a contatos[2]
        // Na X interação do for a variável contato vai ser igual a contatos[X]
        // Como a variável contato é um objeto Contato, é possível acessar seus métodos e atributos
        // então é possível acessar contato.imprimirContato()
        // contato.telefone e etc

        for(Contato contato : contatos){
            contato.imprimirContato();
        }
    }

    public void imprimirEnderecos(){

        for(Endereco endereco : enderecos){
            endereco.imprimirEndereco();
        }
    }
    public void imprimirCliente(){
        System.out.println("Nome: " + nome);
        System.out.println("CPF: " + cpf);
    }

}
