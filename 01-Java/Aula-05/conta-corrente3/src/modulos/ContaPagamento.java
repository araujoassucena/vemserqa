package modulos;

import interfaces.IImpressao;

public class ContaPagamento extends Conta implements IImpressao {

    private static final double TAXA_SAQUE = 4.25;

    public ContaPagamento() {
    }

    public ContaPagamento(String numeroDaConta, Cliente cliente, String agencia, double saldo) {
        super(numeroDaConta, cliente, agencia, saldo);
    }

    @Override
    public void imprimir() {
        this.imprimirContaPagamento();
        this.getCliente().imprimirCliente();
        this.getCliente().imprimirContatos();
        this.getCliente().imprimirEnderecos();
    }

    private void imprimirContaPagamento() {
        System.out.println("=================== Conta Poupanca ===================");
        System.out.println("numero da conta: " +this.getNumeroDaConta());
        System.out.println("agencia: " + this.getAgencia());
        System.out.println("saldo: " + this.getSaldo());
        System.out.println("Taxa saque: " + TAXA_SAQUE);
    }

    @Override
    public boolean sacar(double valor) {
        if (valor <= 0) {
            System.out.println("Não é possível sacar valor menor ou igual a 0");
            return false;
        }

        if (valor > this.getSaldo() - TAXA_SAQUE) {
            System.out.println("Não é possível sacar valor maior que a subtração do saldo - Taxa de Saque");

            return false;
        }

        this.setSaldo(this.getSaldo() - valor - TAXA_SAQUE);
        System.out.println("Foi realizado o saque de: " + valor); //atualizar getSaldo()
        System.out.println("O saldo atual é de: " + this.getSaldo());

        return true;

    }

}
