package modulos;

import interfaces.IMovimentacao;

public abstract class Conta implements IMovimentacao {
    private String numeroDaConta;
    private Cliente cliente;

    private String agencia;

    private double saldo;

    public Conta(){

    }
    public Conta(String numeroDaConta, Cliente cliente, String agencia, double saldo) {
        this.numeroDaConta = numeroDaConta;
        this.cliente = cliente;
        this.agencia = agencia;
        this.saldo = saldo;
    }

    @Override
    public boolean sacar(double valor){
        if(valor <= 0){
            System.out.println("Não é possível sacar valor menor ou igual a 0");
            return false;
        }

        if(valor > this.getSaldo()){
            System.out.println("Não é possível sacar valor maior que a soma do saldo + saque especial");

            return false;
        }

        this.setSaldo(this.getSaldo() - valor);
        System.out.println("Foi realizado o saque de: " + valor); //atualizar getSaldo()
        System.out.println("O saldo atual é de: " + this.getSaldo());

        return true;
    }
    @Override
    public boolean depositar(double valor){
        if(valor <= 0){
            System.out.println("Não é possível depositar valor menor ou igual a 0");
            return false;
        }
        this.saldo = this.saldo + valor;
        System.out.println("Deposito realizado com sucesso!");
        System.out.println("Saldo após o deposito: " + valor);
        return true;

    }
    @Override
    public boolean transferir(Conta conta, double valor){

        boolean sacou = this.sacar(valor);
        if(sacou){
            conta.depositar(valor);
            return true;
        }
        System.out.println("Transferência realizada com sucesso!!");

        return false;
    }

    public String getNumeroDaConta() {
        return numeroDaConta;
    }

    public void setNumeroDaConta(String numeroDaConta) {
        this.numeroDaConta = numeroDaConta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }


}
