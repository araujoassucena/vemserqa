package modulos;

import java.util.ArrayList;

public class Cliente  {

   private String nome;
   private String cpf;
   private ArrayList <Contato> contatos = new ArrayList<>();
   private ArrayList <Endereco> enderecos = new ArrayList<>();

    public void imprimirContatos(){
        for(Contato contato : contatos){
            contato.imprimirContato();
        }
    }

    public void imprimirEnderecos(){
        for(Endereco endereco : enderecos){
            endereco.imprimirEndereco();
        }
    }
    public void imprimirCliente(){
        System.out.println("Nome: " + nome);
        System.out.println("CPF: " + cpf);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public ArrayList<Contato> getContatos() {
        return this.contatos;
    }

    public void setContatos(ArrayList<Contato> contatos) {

       this.contatos = contatos;
    }

    public ArrayList<Endereco> getEnderecos() {

        return this.enderecos;
    }

    public void setEnderecos(ArrayList<Endereco> enderecos) {
       this.enderecos = enderecos;

    }
    public Cliente(){}
    public Cliente(String nome, String cpf, ArrayList<Contato> contatos, ArrayList<Endereco> enderecos) {
        this.nome = nome;
        this.cpf = cpf;
        this.contatos = contatos;
        this.enderecos = enderecos;
    }
}
