import modulos.*;

import java.util.ArrayList;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {

        Contato contatoCliente1 = new Contato("Celular de trabalho", "88888888888", 1);
        Contato contato2Cliente1 = new Contato("Celular de casa", "99999999999", 2);

        Endereco enderecoCliente1 = new Endereco(1, "Rua Exemplo", 123, "Apto 45", "12345-678", "Cidade Exemplo", "EX", "Brasil");
        Endereco endereco2Cliente1 = new Endereco(1, "Rua asdasd", 12223, "Aptoasdasd 45", "15212-678", "Cida1212de Exemplo", "E212X", "Br212asil");

        ArrayList<Contato> listaContatosCliente1 = new ArrayList<Contato>();
        listaContatosCliente1.add(contatoCliente1);
        listaContatosCliente1.add(contato2Cliente1);

        ArrayList<Endereco> listaEnderecoCliente1 = new ArrayList<Endereco>();
        listaEnderecoCliente1.add(enderecoCliente1);
        listaEnderecoCliente1.add(endereco2Cliente1);

        Cliente cliente1 = new Cliente("bella 1", "000.000.000-00", listaContatosCliente1, listaEnderecoCliente1);

        cliente1.imprimirCliente();
        cliente1.imprimirContatos();
        cliente1.imprimirEnderecos();

        ContaCorrente contaCorrenteCliente1 = new ContaCorrente("1234",cliente1, "123", 1000, 300);

        Contato contatoCliente2 = new Contato("Celular de trabalho","88888888888",1 );

        Endereco enderecoCliente2 = new Endereco(1, "Rua Exemplo", 123, "Apto 45", "12345-678", "Cidade Exemplo", "EX", "Brasil");

        ArrayList<Contato> listaContatosCliente2 = new ArrayList<Contato>();
        listaContatosCliente2.add(contatoCliente2);
        listaContatosCliente2.add(contatoCliente2);

        ArrayList<Endereco> listaEnderecoCliente2 = new ArrayList<Endereco>();
        listaEnderecoCliente2.add(enderecoCliente2);
        listaEnderecoCliente2.add(enderecoCliente2);

        Cliente cliente2 = new Cliente("lulu 2","000.000.000-00",listaContatosCliente2,listaEnderecoCliente2 );
        ContaCorrente contaCorrenteCliente2 = new ContaCorrente("4231", cliente2, "431", 500, 50 );

        contaCorrenteCliente1.sacar(1300.0001);

        contaCorrenteCliente1.sacar(-20);

        contaCorrenteCliente1.sacar(300);

        contaCorrenteCliente1.sacar(1300.0001);

        contaCorrenteCliente1.depositar(0);

        contaCorrenteCliente1.depositar(100);

        contaCorrenteCliente1.transferir(contaCorrenteCliente2, 500);

        contaCorrenteCliente1.transferir(contaCorrenteCliente2, 1500);

        Cliente cliente3 = new Cliente("Orochi", "000.000.000-00", listaContatosCliente1, listaEnderecoCliente1);
        Cliente cliente4 = new Cliente("teemo", "000.000.000-00", listaContatosCliente1, listaEnderecoCliente1);

        ContaCorrente contaCorrenteCliente3 = new ContaCorrente("1234",cliente3, "123", 1000, 300);
        ContaPagamento contaPagamentoCliente3 = new ContaPagamento("1668", cliente3, "156", 1000);

        ContaPoupanca contaPoupancaCliente4 = new ContaPoupanca("1897", cliente4, "167", 2000);

        contaPagamentoCliente3.sacar(1000);

        contaPagamentoCliente3.sacar(950);

        contaPagamentoCliente3.depositar(954.25);
        contaPagamentoCliente3.sacar(995.75);
        contaPagamentoCliente3.depositar(1000);

        contaPagamentoCliente3.transferir(contaPoupancaCliente4, 500);

        contaPoupancaCliente4.depositar(3000);
        contaPoupancaCliente4.creditarTaxa();

        contaPoupancaCliente4.transferir(contaCorrenteCliente3, 1000);

        contaPoupancaCliente4.imprimir();
        contaCorrenteCliente3.imprimir();
        contaPagamentoCliente3.imprimir();


    }

}