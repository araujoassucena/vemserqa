package interfaces;

import modulos.Conta;

public interface IMovimentacao {


    boolean depositar(double valor);

   boolean transferir (Conta conta, double valor);

    boolean sacar(double valor);
}
