import java.util.Scanner;

public class Exercicio04 {
    public static void main(String[] args) {

        int a = 10;
        int b = 20;

        System.out.println("O valor de A é " + a);
        System.out.println("O valor de B é " + b);

        int auxiliar = a;
        a = b;
        b = auxiliar;

        System.out.println("O valor de A é " + a);
        System.out.println("O valor de B é " + b);

    }
}
