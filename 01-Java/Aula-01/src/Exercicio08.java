import java.util.Scanner;

public class Exercicio08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Digite o salario");
        Double salario = sc.nextDouble();

        System.out.println("Digite o cargo");
        String cargo = sc.next();
        Double salarioNovo, diferencaSalarios;
        switch (cargo) {

            case "Gerente":
                salarioNovo = salario * 1.1;
                diferencaSalarios = salarioNovo - salario;
                System.out.println("Seu antigo salario é " + salario);
                System.out.println("Seu novo salario é " + salarioNovo);
                System.out.println("A diferença é de é " + diferencaSalarios);

                break;
            case "Engenheiro":
                salarioNovo = salario * 1.2;
                diferencaSalarios = salarioNovo - salario;
                System.out.println("Seu antigo salario é " + salario);
                System.out.println("Seu novo salario é " + salarioNovo);
                System.out.println("A diferença é de é " + diferencaSalarios);

                break;
            case "Tecnico":
                salarioNovo = salario * 1.3;
                diferencaSalarios = salarioNovo - salario;
                System.out.println("Seu antigo salario é" + salario);
                System.out.println("Seu novo salario é" + salarioNovo);
                System.out.println("A diferença é de é" + diferencaSalarios);

                break;

            default:
                salarioNovo = salario * 1.4;
                diferencaSalarios = salarioNovo - salario;
                System.out.println("Seu antigo salario é " + salario);
                System.out.println("Seu novo salario é " + salarioNovo);
                System.out.println("A diferença é de é " + diferencaSalarios);

                break;
        }

    }
}
