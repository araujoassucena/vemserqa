import java.util.Scanner;

public class Exercicio07 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Digite o codigo");
        String codigo = sc.next();

        System.out.println("Digite a quantidade");
        int quantidade = sc.nextInt();
        System.out.println(codigo);
        switch (codigo) {

            case "ABCD":
                System.out.println("O total devido é " + 5.30 * quantidade);
                break;
            case "XYPK":
                System.out.println("O total devido é " + 6.00 * quantidade);
                break;
            case "KLMP":
                System.out.println("O total devido é " + 3.20 * quantidade);
                break;
            case "QRST":
                System.out.println("O total devido é " + 2.50 * quantidade);
                break;
            default:
                System.out.println("Codigo informado não existe");
                break;
        }

    }
}
