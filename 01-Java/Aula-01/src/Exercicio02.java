import java.util.Scanner;

public class Exercicio02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Escolha um número referente ao estado que deseja selecionar");
        System.out.println("1 - Ceara");
        System.out.println("2 - Para");
        System.out.println("3 - Rio de Janeiro");

        int escolhaEstado = sc.nextInt();

        if(escolhaEstado == 1){
            System.out.println("Escolha um número referente a cidade que deseja selecionar");
            System.out.println("1 - Quixada");
            System.out.println("2 - Fortaleza");

            int escolhaCidade = sc.nextInt();

            if(escolhaCidade == 1){
                System.out.println("Informações de Quixadá");
                System.out.println("População: 88.321 (2020)");
                System.out.println("O Índice de Desenvolvimento Humano (IDH) municipal é de 0,659, segundo o IBGE.");
            }else if(escolhaCidade == 2){
                System.out.println("Informações de Fortaleza");
                System.out.println("População: 2,687 milhões (2020)");
                System.out.println("Em Fortaleza e Região Metropolitana o IDHM de Educação, em 2000, era 0,488, passando, em 2010, para 0,672.");
            }
        }else if(escolhaEstado == 2){
            System.out.println("Escolha um número referente a cidade que deseja selecionar");
            System.out.println("1 - Belem");
            System.out.println("2 - Braganca");

            int escolhaCidade = sc.nextInt();

            if(escolhaCidade == 1){
                System.out.println("Informações de Blém,");
                System.out.println("População: 1.303.403 (2020)");
                System.out.println("O Índice de Desenvolvimento Humano (IDH) municipal é de 0,746, segundo o IBGE.");
            }else if(escolhaCidade == 2){
                System.out.println("Informações de Braganca");
                System.out.println("População: 128.914 (2020)");
                System.out.println("Em Braganca e Região Metropolitana o IDHM de Educação, em 2000, era 0,6, passando, em 2010, para 0,672.");
            }
        }else if(escolhaEstado == 3){
            System.out.println("Escolha um número referente a cidade que deseja selecionar");
            System.out.println("1 - Niteroi");
            System.out.println("2 - Petropolis");

            int escolhaCidade = sc.nextInt();

            if(escolhaCidade == 1){
                System.out.println("Informações de Niteroi");
                System.out.println("População: 515.317 (2020)");
                System.out.println("O Índice de Desenvolvimento Humano (IDH) municipal é de 0,886, segundo o IBGE.");
            }else if(escolhaCidade == 2){
                System.out.println("Informações de Petropolis");
                System.out.println("População: 306.678 (2020)");
                System.out.println("Em Petropolis e Região Metropolitana o IDHM de Educação, em 2000, era 0,745, passando, em 2010, para 0,672.");
            }
        }
    }
}
