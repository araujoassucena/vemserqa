import java.util.Scanner;

public class Exercicio06 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Olá, digite o total de eleitores");
        double totalEleitores = sc.nextDouble();

        System.out.println("Olá, digite o total de votos brancos");
        double votosBrancos = sc.nextDouble();


        System.out.println("Olá, digite o total de votos nulos");
        double votosNulos = sc.nextDouble();


        System.out.println("Olá, digite o total de votos validos");
        double votosValidos = sc.nextDouble();

        double percentualVotosBrancos = (votosBrancos / totalEleitores) * 100;
        double percentualVotosNulos = (votosNulos / totalEleitores) * 100;
        double percentualVotosValidos = (votosValidos / totalEleitores) * 100;


        System.out.println("O percentual de votos brancos é: " + percentualVotosBrancos + "%");
        System.out.println("O percentual de votos nulos é: " + percentualVotosNulos + "%");
        System.out.println("O percentual de votos validos é: " + percentualVotosValidos + "%");

    }
}
