import java.util.Scanner;

public class Exercicio09 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Digite a hora de inicio");
        int horaInicio = sc.nextInt();
        System.out.println("Digite os minutos de inicio");
        int minutosInicio = sc.nextInt();
        System.out.println("Digite a hora de fim");
        int horaFim = sc.nextInt();
        System.out.println("Digite os minutos de fim");
        int minutosFim = sc.nextInt();


        if(horaInicio>horaFim){
           horaFim += 24;
        }

        int horaInicioEmMinutos = (horaInicio*60) + (minutosInicio);
        int horaFimEmMinutos = (horaFim*60) + (minutosFim);
        int diferencaHorasEmMinutos = horaFimEmMinutos - horaInicioEmMinutos;

        int horas = diferencaHorasEmMinutos/60;
        int minutos = diferencaHorasEmMinutos%60;

        System.out.println("tempo de duração em horas " + horas);
        System.out.println("tempo de duração em minutos " + minutos);

    }

}
