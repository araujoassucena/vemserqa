import java.util.Scanner;

public class Exercicio05 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Digite a base do triangulo");
        int base = sc.nextInt();
        System.out.println("Digite a altura do triangulo");
        int altura = sc.nextInt();

        System.out.println("A area do triangulo é: " + (base*altura)/2);

    }
}
