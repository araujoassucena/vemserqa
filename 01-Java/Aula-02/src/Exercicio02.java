import java.util.Scanner;

public class Exercicio02 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        //faça um programa que tenha a mesma ideia, você informará um
        //número e quando alguém for tentar adivinhá-lo o programa deverá informar se
        //acertou o número ou se errou, se errou o programa deverá apresentar uma das
        //seguintes frases: O número a ser encontrado é menor do que você digitou ou O
        //número a ser encontrado é maior do que você digitou.
        //OBS: o programa deve parar quando a pessoa acertar o número adivinhado.

        System.out.println("Informe o numero!");
        int numero = sc.nextInt();
        boolean acertou = false;

        while (!acertou) {
            System.out.println("Tente advinhar o numero informado!");

            int numeroAdivinha = sc.nextInt();

            if (numeroAdivinha == numero) {
                System.out.println("Acertou!!");
                acertou = true;

            } else if (numeroAdivinha > numero) {
                System.out.println("Numero a ser encontrado é menor");
            } else {
                System.out.println("Numero a ser encontrado é maior");
            }


        }
    }
}
