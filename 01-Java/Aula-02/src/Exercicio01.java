import java.util.Scanner;
import java.util.regex.Matcher;

public class Exercicio01 {
    public static void main(String[] args) {

        //. Elabore um programa para uma papelaria que leia o nome e o valor de um produto.
        //Em seguida, faça uma lista de 1 a 10 unidades do produto, sendo que o desconto de
        //uma unidade desse produto seja de 5% e de 2 seja 10% de 3 = 15% etc... Até
        //alcançar os 50% de desconto
        //para entender melhor, um exemplo:
        //Produto.: lápis
        //Preço R$.: 2,00
        //Promoção: lápis
        //------------------------
        //1 x R$ 1,90 = R$ 1,90
        //2 x R$ 1,80 = R$ 3,60
        //3 x R$ 1,70 = R$ 5,10
        //4 x R$ 1,60 = R$ 6,40
        //5 x R$ 1,50 = R$ 7,50
        //6 x R$ 1,40 = R$ 8,40
        //7 x R$ 1,30 = R$ 9,10
        //8 x R$ 1,20 = R$ 9,60
        //9 x R$ 1,10 = R$ 9,90
        //10 x R$ 1,00 = R$ 10,00

        Scanner sc = new Scanner(System.in);

        System.out.println("Digite o nome do produto! ");
        String nomeProduto = sc.next();

        System.out.println("Digite o valor do produto! ");
        double valorDoProduto = sc.nextDouble();

        String[] lista= new String[10];

        // salva as informações na lista
        for (int i = 0; i < lista.length ; i++) {
            double desconto=(0.95 - (0.05*i));
            double produtoComDesconto = valorDoProduto * desconto;
            double total= (i+1)*produtoComDesconto;
            lista[i] = i +1 + " x R$ " + produtoComDesconto +" = R$ " + total;


            if(desconto <= 0.5){
                break;
            }

        }
        System.out.println("Produto.: " + nomeProduto);
        System.out.println("Preço R$.:" + valorDoProduto);
        System.out.println("Promoção.: " + nomeProduto);

        // exibe as informações
        for (String prodo : lista) {
            System.out.println(produto);

        }


    }
}
