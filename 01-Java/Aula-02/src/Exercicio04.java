import java.util.Scanner;

public class Exercicio04 {
    public static void main(String[] args) {

        // Faça um programa que leia uma matriz de 5x4 inteiros contendo as seguintes
        //informações do tipo inteiro:
        //a. Primeira coluna: número da matrícula
        //b. Segunda coluna: media das provas
        //c. Terceira coluna: média dos trabalhos
        //d. Quarta coluna: nota final
        //Elabore um programa que:
        //a. Leia as 3 primeiras informações de cada aluno
        //b. Calcule a nota final = (media das provas * 0,6 + media dos trabalhos * 0,4)
        //c. Imprima a matrícula que obteve a maior nota final
        //d. Imprima a média das notas finais

        Scanner sc = new Scanner(System.in);

        int[][] alunos = new int[5][4];

        int maiorNota = 0;
        int matriculaComMaiorNota = 0;
        int mediaNotasFinais =0;

        for (int linha = 0; linha < 5; linha++) {
            System.out.println("digite a matricula");
            int matricula = sc.nextInt();
            alunos[linha][0] = matricula;

            System.out.println("digite a media das provas");
            int mediaProva = sc.nextInt();
            alunos[linha][1] = mediaProva;

            System.out.println("digite a media dos trabalhos");
            int mediaTrabalho = sc.nextInt();
            alunos[linha][2] = mediaTrabalho;

            int notaFinal = (int) ((mediaProva * 0.6) + (mediaTrabalho * 0.4));

            alunos[linha][3] = notaFinal;

            if(notaFinal > maiorNota){
                maiorNota = notaFinal;
                matriculaComMaiorNota = matricula;
            }

            mediaNotasFinais = notaFinal + mediaNotasFinais;
        }
        mediaNotasFinais = mediaNotasFinais/5;
        System.out.println("A matrícula com maior nota é: " + matriculaComMaiorNota);
        System.out.println("A nota final do aluno com maior nota é: " +maiorNota);
        System.out.println("A media das notas finais é: " +mediaNotasFinais);
    }
}
