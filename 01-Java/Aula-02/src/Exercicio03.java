import java.util.Scanner;

public class Exercicio03 {
    public static void main(String[] args) {
        //Desenvolver um algoritmo que peça nome, altura, idade, peso dos jogadores de
        //basquete, enquanto o nome do jogador for diferente da palavra SAIR o programa
        //deverá pedir essas informações, após cadastrados deverá aparecer as seguintes
        //informações:
        //Quantidade de jogadores cadastrados;
        //Altura do maior Jogador;
        //Jogador mais velho;
        //Jogador mais pesado;
        //Média das alturas jogadores

        Scanner sc = new Scanner(System.in);

        double maiorAltura = 0;
        int maisVelho = 0;
        double maisPesado = 0;
        double mediaAltura = 0;
        int quantidade = 0;

        while (true) {
            System.out.println("Digite o nome do jogador!");


            System.out.println("caso deseje finalizar o cadastro, digite: Sair");
            String nome = sc.next();
            if(nome.equalsIgnoreCase("SAIR"))   break;

            System.out.println("Digite a altura do jogador!");
            double altura = sc.nextDouble();
            System.out.println("Digite a idade do jogador!");
            int idade = sc.nextInt();
            System.out.println("Digite o peso do jogador!");
            double peso = sc.nextDouble();

            quantidade++; //somar a quantidade de jogadores cadastrados

            if(altura > maiorAltura){
                maiorAltura = altura;
            }

            if(idade > maisVelho){     // substitue o anterior
                maisVelho = idade;
            }

            if(peso > maisPesado){
                maisPesado = peso;
            }

            mediaAltura = mediaAltura + altura;

        }

        System.out.println("Quantidade de jogadores cadastrados: " + quantidade);
        System.out.println("A altura do maior jogador: " + maiorAltura);
        System.out.println("Jogador mais velho: " + maisVelho);
        System.out.println("Jogador mais pesado: " + maisPesado);
        System.out.println("Media das alturas dos jogadores: " + (mediaAltura/quantidade));
    }


}
