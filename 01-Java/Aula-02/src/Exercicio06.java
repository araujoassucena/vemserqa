import java.util.Scanner;

public class Exercicio06 {
    public static void main(String[] args) {

        //Desenvolver um vetor que contenha 10 números (não será pedido ao usuário). Com
        //esse vetor faça:
        //a. Peça um número ao usuário e armazene-o;
        //b. Imprima quantas vezes o número digitado existe no vetor;
        //c. Imprima quantos números menores que o número digitado;
        //d. Imprima quantos números maiores que o número digitado;

        Scanner sc = new Scanner(System.in);
        int[] numeros= {1, 2, 3, 4, 5, 5, 7, 8, 9, 10};

        System.out.println(" Escolha um numero de 1 a 10");
        int numeroEscolhido = sc.nextInt();

        int quantidadeNumerosIguais = 0;
        int quantidadeNumerosMenores = 0;
        int quantidadeNumerosMaiores = 0;

        for (int valor = 0; valor < 10; valor++) {


            if(numeros[valor] == numeroEscolhido){

                quantidadeNumerosIguais++;

            }else if(numeros[valor] < numeroEscolhido){
                quantidadeNumerosMenores++;
            }else{
                quantidadeNumerosMaiores++;
            }


        }
        System.out.println("A quantidade de vezes que o numero aparece no vetor é: " +quantidadeNumerosIguais);
        System.out.println("A quantidade de numeros menores é: " + quantidadeNumerosMenores);
        System.out.println("A quantidade de numeros maiores é: " + quantidadeNumerosMaiores);

    }
}
