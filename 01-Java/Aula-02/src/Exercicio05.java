import java.util.Scanner;

public class Exercicio05 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //Receba uma matriz 10x3 onde contém 10 itens de supermercado (linhas) que foram
        //cotados em 3 mercados diferentes (colunas). Com esses dados, escreva um algoritmo
        //que calcule qual é o mercado mais barato para se comprar considerando todos os
        //preços listados

        int[][] lista = new int[10][3];

        int totalMercado1 = 0;
        int totalMercado2 = 0;
        int totalMercado3 = 0;

        for (int linha = 0; linha < 10; linha++) {
            System.out.println("Digite o preco do produto no supermercado 1");
            lista[linha][0] = sc.nextInt();

            System.out.println("Digite o preco do produto no supermercado 2");
            lista[linha][1] = sc.nextInt();

            System.out.println("Digite o preco do produto no supermercado 3");
            lista[linha][2] = sc.nextInt();
        }

        for (int linha = 0; linha < 10; linha++) {   //total do valor de cada mercado, percorrendo os valores.
            totalMercado1 = totalMercado1 + lista[linha][0];
            totalMercado2 = totalMercado2 + lista[linha][1];
            totalMercado3 = totalMercado3 + lista[linha][2];


        }

        if (totalMercado1 < totalMercado2
                && totalMercado1 < totalMercado3) {
            System.out.println("Mercado 1 é mais barato");
        } else if (totalMercado2 < totalMercado1
                && totalMercado2 < totalMercado3) {
            System.out.println("Mercado 2 é mais barato");
        } else if (totalMercado3 < totalMercado1
                && totalMercado3 < totalMercado2) {
            System.out.println("Mercado 3 é mais barato");
        } else if (totalMercado1 == totalMercado2
                && totalMercado1 < totalMercado3) {
            System.out.println("Mercado 1 e 2 são mais barato");
        } else if (totalMercado2 < totalMercado1
                && totalMercado2 == totalMercado3) {
            System.out.println("Mercado 2 e 3 são mais barato");
        } else if (totalMercado3 == totalMercado1
                && totalMercado3 < totalMercado2) {
            System.out.println("Mercado 3 e 1 são mais barato");
        } else if (totalMercado3 == totalMercado1
                && totalMercado2 == totalMercado3) {
            System.out.println("Mercado 1, 2 e 3 são igualmente baratos barato");
        }
    }
}
