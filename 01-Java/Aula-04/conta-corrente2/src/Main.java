import modulos.Cliente;
import modulos.ContaCorrente;
import modulos.Contato;
import modulos.Endereco;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {


        Contato contatoCliente1 = new Contato("Celular de trabalho", "88888888888", 1);
        Contato contato2Cliente1 = new Contato("Celular de casa", "99999999999", 2);

        Endereco enderecoCliente1 = new Endereco(1, "Rua Exemplo", 123, "Apto 45", "12345-678", "Cidade Exemplo", "EX", "Brasil");
        Endereco endereco2Cliente1 = new Endereco(1, "Rua asdasd", 12223, "Aptoasdasd 45", "15212-678", "Cida1212de Exemplo", "E212X", "Br212asil");

        Contato[] listaContatosCliente1 = new Contato[]{contatoCliente1, contato2Cliente1};
        Endereco[] listaEnderecosCliente1 = new Endereco[]{enderecoCliente1, endereco2Cliente1};

        Cliente cliente1 = new Cliente("bella 1", "000.000.000-00", listaContatosCliente1, listaEnderecosCliente1);


        cliente1.imprimirCliente();
        cliente1.imprimirContatos();
        cliente1.imprimirEnderecos();

        ContaCorrente contaCorrenteCliente1 = new ContaCorrente("1234",cliente1, "123", 1000, 300);


        Contato contatoCliente2 = new Contato("Celular de trabalho","88888888888",1 );


        Endereco enderecoCliente2 = new Endereco(1, "Rua Exemplo", 123, "Apto 45", "12345-678", "Cidade Exemplo", "EX", "Brasil");

        Contato[] listaContatoCliente2 = new Contato[]{contatoCliente2};
        Endereco[] listaEnderecosCliente2 = new Endereco[]{enderecoCliente2};


        Cliente cliente2 = new Cliente("lulu 2","000.000.000-00",listaContatoCliente2,listaEnderecosCliente2 );
        ContaCorrente contaCorrenteCliente2 = new ContaCorrente("4231", cliente2, "431", 500, 50 );



        System.out.println("===================");

        // tentar sacar mais de 1300 em uma conta com saldo de 1000 e 300 de cheque
        boolean sacou = contaCorrenteCliente1.sacar(1300.0001);
        System.out.println("foi possível sacar 1300.0001 de 1000 com 300 de cheque especial? " + sacou);

        // sacar valor negativo
        boolean sacou2 = contaCorrenteCliente1.sacar(-20);
        System.out.println("foi possível sacar -20? " + sacou2);

        // sacar valor válido
        boolean sacou3 = contaCorrenteCliente1.sacar(300);
        System.out.println("foi possível sacar 300 com 1000 de saldo e 300 de cheque especial? " + sacou3);
        System.out.println("O saldo foi atualizado após sacar 300? " + contaCorrenteCliente1.getSaldo());

        // sacar valor alto após o saque
        boolean sacou4 = contaCorrenteCliente1.sacar(1300.0001);
        System.out.println("foi possível sacar 1300.0001 após realizar um saque anteriormente? " + sacou4);

        // depositar 0 reais
        boolean depositar = contaCorrenteCliente1.depositar(0);
        System.out.println("foi possível depositar 0? " + depositar);

        // depositar 100
        boolean depositar1 = contaCorrenteCliente1.depositar(100);
        System.out.println("foi possível depositar 0? " + depositar1);
        System.out.println("O saldo foi atualizado após depositar 100? " + contaCorrenteCliente1.getSaldo());


        System.out.println("total cliente 1 " + contaCorrenteCliente1.getSaldo());
        System.out.println("total cliente 2 " + contaCorrenteCliente2.getSaldo());

        // transferir 500 reais para a conta corrente 2 com sucesso
        boolean transferiu = contaCorrenteCliente1.transferir(contaCorrenteCliente2, 500);

        System.out.println("foi possível transferir 500 de cliente 1 para cliente2? " + transferiu);
        System.out.println("total cliente 1 " + contaCorrenteCliente1.getSaldo());
        System.out.println("total cliente 2 " + contaCorrenteCliente2.getSaldo());

        // tentar transferir 1500 sem ter dinheiro em saldo
        boolean transferir1 = contaCorrenteCliente1.transferir(contaCorrenteCliente2, 1500);
        System.out.println("foi possível transferir 1500 com 500 de saldo e 300 de cheque especial? " + transferir1);
        System.out.println("total cliente 1 " + contaCorrenteCliente1.getSaldo());
        System.out.println("total cliente 2 " + contaCorrenteCliente2.getSaldo());

        contaCorrenteCliente1.imprimir();
        contaCorrenteCliente2.imprimir();


    }

}