package modulos;

import interfaces.IImpressao;

public class ContaCorrente extends Conta implements IImpressao {

    private double chequeEspecial;
    
    private void imprimirContaCorrente(){

        System.out.println("=================== Conta corrente ===================");
        System.out.println("numero da conta: " +this.getNumeroDaConta());
        System.out.println("agencia: " + this.getAgencia());
        System.out.println("saldo: " + this.getSaldo());
        System.out.println("cheque especial: " + this.getChequeEspecial());
    }
    @Override
    public boolean sacar(double valor){
        if(valor <= 0){
            System.out.println("Não é possível sacar valor menor ou igual a 0");
            return false;
        }

        if(valor > this.getSaldo() + this.getChequeEspecial()){
            System.out.println("Não é possível sacar valor maior que a soma do saldo + saque especial");

            return false;
        }

        this.setSaldo(this.getSaldo() - valor);
        System.out.println("Foi realizado o saque de: " + valor); //atualizar getSaldo()
        System.out.println("O saldo atual é de: " + this.getSaldo());

        return true;
    }

    public double retornarSaldoComChequeEspecial(){
        return this.getSaldo() + this.chequeEspecial;
    }

    @Override
    public void imprimir() {
        this.imprimirCliente();
        this.imprimirContato();
        this.imprimirEndereco();
        this.imprimirContaCorrente();
    }


    public double getChequeEspecial() {
        return chequeEspecial;
    }

    public void setChequeEspecial(double chequeEspecial) {
        this.chequeEspecial = chequeEspecial;
    }

    public ContaCorrente(){}
    public ContaCorrente(double chequeEspecial) {
        this.chequeEspecial = chequeEspecial;
    }

    public ContaCorrente(String numeroDaConta, Cliente cliente, String agencia, double saldo, double chequeEspecial) {
        super(numeroDaConta, cliente, agencia, saldo);
        this.chequeEspecial = chequeEspecial;
    }
}
