package modulos;

import interfaces.IMovimentacao;

public class Conta implements IMovimentacao {
    private String numeroDaConta;
    private Cliente cliente;

    private String agencia;

    private double saldo;

    public Conta(){

    }
    public Conta(String numeroDaConta, Cliente cliente, String agencia, double saldo) {
        this.numeroDaConta = numeroDaConta;
        this.cliente = cliente;
        this.agencia = agencia;
        this.saldo = saldo;
    }

    public void imprimirContato(){

    }

    public void imprimirEndereco(){

    }
    public void imprimirCliente(){

    }
    @Override
    public boolean sacar(double valor){
        return true;
    }
    @Override
    public boolean depositar(double valor){
        if(valor <= 0){
            System.out.println("Não é possível depositar valor menor ou igual a 0");
            return false;
        }
        this.saldo = this.saldo + valor;
        return true;

    }
    @Override
    public boolean transferir(Conta conta, double valor){

        boolean sacou = this.sacar(valor);
        if(sacou){
            conta.depositar(valor);
            return true;
        }

        return false;
    }

    public String getNumeroDaConta() {
        return numeroDaConta;
    }

    public void setNumeroDaConta(String numeroDaConta) {
        this.numeroDaConta = numeroDaConta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }


}
