package modulos;

import interfaces.IImpressao;

public class ContaPoupanca extends Conta implements IImpressao {

    private static final double JUROS_MENSAL = 1.01;

    public void creditarTaxa(){
        this.setSaldo(this.getSaldo()* JUROS_MENSAL);
    }

    private void imprimirContaPoupanca(){

        System.out.println("=================== Conta corrente ===================");
        System.out.println("numero da conta: " +this.getNumeroDaConta());
        System.out.println("agencia: " + this.getAgencia());
        System.out.println("saldo: " + this.getSaldo());
        System.out.println("Taxa juros mensal: " + JUROS_MENSAL);
    }
    @Override
    public void imprimir() {

        this.imprimirCliente();
        this.imprimirContato();
        this.imprimirEndereco();
        this.imprimirContaPoupanca();
    }

}
