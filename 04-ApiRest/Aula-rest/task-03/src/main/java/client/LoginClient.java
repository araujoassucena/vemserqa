package client;

import io.restassured.http.ContentType;
import model.Login;
import specs.InicialSpecs;

import static io.restassured.RestAssured.given;

public class LoginClient {
    private static final String LOGIN = "/login";

   public String logar (Login bodyLogin){
       return
               given()
                       .log().all()
                       .spec(InicialSpecs.setup())
                       .body(bodyLogin)
                       .contentType(ContentType.JSON)
               .when()
                       .post(LOGIN)
               .then()
                       .log().all()
                       .extract().path("authorization");
   }



}
