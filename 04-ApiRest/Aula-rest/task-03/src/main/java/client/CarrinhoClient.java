package client;

import io.restassured.response.Response;
import model.CarrinhoReq;
import model.ProdutoReq;
import specs.ProdutoSpecs;
import utils.Autenticacao;

import static io.restassured.RestAssured.given;

public class CarrinhoClient {

    private static final String CARRINHO = "/carrinhos";
    private static final String DELETECARRINHO = "/carrinhos/cancelar-compra";


    public static Response cadastroProdutoCarrinho (CarrinhoReq carrinho  ){
        return

                given()
                        .spec(ProdutoSpecs.produtoSpec())
                        .header("Authorization", Autenticacao.tokenAdm())
                        .body(carrinho)
                        .log().all()
                .when()
                        .post(CARRINHO);
}
   public static Response excluirCarrinhoCancelandoCompra(){
        return
                given()
                        .spec(ProdutoSpecs.produtoSpec())
                        .header("Authorization", Autenticacao.tokenAdm())
                        .log().all()
                .when()
                        .delete(DELETECARRINHO);

   }

}

