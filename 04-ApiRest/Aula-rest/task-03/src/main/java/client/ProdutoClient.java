package client;
import io.restassured.response.Response;
import model.ProdutoReq;
import specs.InicialSpecs;
import specs.ProdutoSpecs;
import utils.Autenticacao;

import static io.restassured.RestAssured.given;

public class ProdutoClient {
    public ProdutoClient() {}
    private static final String PRODUTO = "/produtos";
    private static final String PRODUTO_ID = "/produtos/{_id}";


    public static Response cadastroProduto (ProdutoReq produto ){
       return

                given()
                        .spec(ProdutoSpecs.produtoSpec())
                        .header("Authorization", Autenticacao.tokenAdm())
                        .body(produto)

                .when()
                        .post(PRODUTO);
    }

    public static Response cadastroProdutoNaoAdm (ProdutoReq produto ){
        return

                given()
                        .spec(ProdutoSpecs.produtoSpec())
                        .header("Authorization", Autenticacao.tokenNaoAdm())
                        .body(produto)

                        .when()
                        .post(PRODUTO);
    }


    public static Response deletarProduto (String id ){
        return

                given()  //Deletar a massa
                        .log().all()
                        .spec(ProdutoSpecs.produtoSpec())
                        .pathParam("_id",id)
                        .header("Authorization", Autenticacao.tokenAdm())
                .when()
                        .delete(PRODUTO_ID);
    }

    public static Response buscarProdutoPorId (String id){
        return
                given()
                        .spec(InicialSpecs.setup())
                        .pathParam("_id", id)
                .when()
                        .get(PRODUTO_ID);

    }

    public static Response editarProdutoPorId (ProdutoReq produtoReq, String id){
        return

                given()
                        .log().all()
                        .spec(ProdutoSpecs.produtoSpec())
                        .pathParam("_id", id)
                        .header("Authorization", Autenticacao.tokenAdm())
                        .body(produtoReq)
                .when()
                        .put(PRODUTO_ID);
    }

    public static Response editarProdutoPorIdNaoAdm (ProdutoReq produtoReq, String id){
        return

                given()
                        .log().all()
                        .spec(ProdutoSpecs.produtoSpec())
                        .pathParam("_id", id)
                        .header("Authorization", Autenticacao.tokenNaoAdm())
                        .body(produtoReq)
               .when()
                        .put(PRODUTO_ID);
    }

    public static Response deletarProdutoNaoAdm (String id ){
        return

                given()  //Deletar a massa
                        .log().all()
                        .spec(ProdutoSpecs.produtoSpec())
                        .pathParam("_id",id)
                        .header("Authorization", Autenticacao.tokenNaoAdm())
                .when()
                        .delete(PRODUTO_ID);
    }


}
