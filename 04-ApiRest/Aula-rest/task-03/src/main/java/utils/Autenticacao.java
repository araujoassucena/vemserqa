package utils;

import client.LoginClient;
import dataFactory.ProdutoDataFactory;
import model.Login;

public class Autenticacao {

    private static final LoginClient loginClient = new LoginClient();


    public static String tokenAdm() {

        Login loginAdm = new Login(
                Manipulacao.getProp().getProperty("emailAdm"),
                Manipulacao.getProp().getProperty("senhaAdm")
        );

        return loginClient.logar(loginAdm);

    }
    public static String tokenNaoAdm() {

        Login loginNaoAdm = new Login(
                Manipulacao.getProp().getProperty("emailNaoAdm"),
                Manipulacao.getProp().getProperty("senhaNaoAdm")
        );

        return loginClient.logar(loginNaoAdm);

    }


}
