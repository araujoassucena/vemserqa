package dataProvider;

import dataFactory.ProdutoDataFactory;
import net.datafaker.Faker;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.groovy.util.StringUtil;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class ProdutoDataProvider {

    private static final String ERRONOMEVAZIO = "nome não pode ficar em branco";
    private static final String ERRODESCRICAOVAZIA = "descricao não pode ficar em branco";
    private static final String ERROQUANTIDADEMENORQUEZERO = "quantidade deve ser maior ou igual a 0";
    private static final String ERR0PRECOMENORQUEZERO = "preco deve ser um número positivo";
    private static final String ERROCAMPOPRECO = "preco deve ser um número";
    private static final String ERROCAMPOQUANTIDADE = "quantidade deve ser um número";

    private static final String ERROIDINVALIDO = "Produto não encontrado";
    private static final String KEY_NOME = "nome";
    private static final String KEY_PRECO = "preco";
    private static final String KEY_DESCRICAO = "descricao";
    private static final String KEY_QUANTIDADE = "quantidade";


    public static Stream<Arguments> dataProviderIdsInvalidos() {
        return Stream.of(
                Arguments.of(ProdutoDataFactory.produtoIdInvalidoComNumeros(), ERROIDINVALIDO),
                Arguments.of(ProdutoDataFactory.produtoIdInvalidoComLetras(), ERROIDINVALIDO),
                Arguments.of(ProdutoDataFactory.produtoIdInvalidoComEspacos(), ERROIDINVALIDO)
        );
    }

    public static Stream<Arguments> dataProviderCadastroInvalido(){
        return Stream.of(
                Arguments.of(ProdutoDataFactory.produtoComNomeVazio(), KEY_NOME, ERRONOMEVAZIO),
                Arguments.of( ProdutoDataFactory.produtoComPrecoMenorQueZero(),KEY_PRECO, ERR0PRECOMENORQUEZERO),
                Arguments.of( ProdutoDataFactory.produtoComLetraNoPreco(), KEY_PRECO, ERROCAMPOPRECO),
                Arguments.of( ProdutoDataFactory.produtoComDescricaoVazio(), KEY_DESCRICAO,ERRODESCRICAOVAZIA),
                Arguments.of( ProdutoDataFactory.produtoComQuantidadeMenorQueZero(), KEY_QUANTIDADE,ERROQUANTIDADEMENORQUEZERO),
                Arguments.of( ProdutoDataFactory.produtoComLetraNaQuantidade(), KEY_QUANTIDADE, ERROCAMPOQUANTIDADE)


        );
    }
}
