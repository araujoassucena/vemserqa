package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.ProdutoReq;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProdutoRes extends ProdutoReq {
    private String message;
    private  String _id;


    @Override
    public String toString() {
        return "ProdutoRes{" +
                "message='" + message + '\'' +
                ", _id='" + _id + '\'' +
                '}';
    }




}
