package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
@Data
@AllArgsConstructor
public class CarrinhoProdutoRes {
    private String message;
    private ArrayList<String> idCarrinhos;

    public CarrinhoProdutoRes(){
        idCarrinhos = new ArrayList<>();}
}
