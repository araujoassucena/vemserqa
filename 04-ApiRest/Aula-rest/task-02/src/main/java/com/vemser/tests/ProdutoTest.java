package com.vemser.tests;

import com.vemser.produto.ProdutoRes;
import net.datafaker.Faker;
import com.vemser.produto.ProdutoReq;
import io.restassured.http.ContentType;
import org.junit.*;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import java.util.Locale;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;


public class ProdutoTest {

    Faker faker = new Faker(new Locale("PT-BR"));

    String urlLogin = "http://localhost/login";
    String bodyLoginADM = """
            {
              "email": "assucena@gmail.com",
              "password": "1233"
            }
            """;
    String bodyLoginNaoAdm = """
            {
              "email": "teste00@gmail.com",
              "password": "1233"
            }""";
    String tokenAdm;
    String tokenNaoAdm;

    @Before
    public void setup() {
        baseURI = "http://localhost";
        port = 3000;

        tokenAdm =
        given()
                .body(bodyLoginADM)
                .contentType(ContentType.JSON)
        .when()
                .post(urlLogin)
        .then()
                .extract()
                .path("authorization");

        tokenNaoAdm =
        given()
                        .body(bodyLoginNaoAdm)
                        .contentType(ContentType.JSON)
        .when()
                        .post(urlLogin)
        .then()
                        .extract()
                        .path("authorization");
    }

    @Test
    public void testCadastrarProdutoComSucesso() {
        ProdutoReq produtoCadastro = produto();
        System.out.println(tokenAdm);
        ProdutoRes produtoRes =
                given()
                        .log().all()
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoCadastro)
                .when()
                        .post("/produtos")
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);

        Assertions.assertEquals("Cadastro realizado com sucesso", produtoRes.getMessage());
        Assertions.assertNotNull(produtoRes.get_id());


                given()  //Deletar a massa
                        .pathParam("id", produtoRes.get_id())
                        .header("Authorization", tokenAdm)
                .when()
                        .delete("/produtos/{id}")
                 .then().log().all()
                        .statusCode(200);

    }

    @Test
    public void testTentarCadastrarProdutoComNomeJaUtilizado() {
        ProdutoReq produtoCadastro = produto();
        ProdutoReq produtoNomeRepetido = produto();
        produtoNomeRepetido.setNome(produtoCadastro.getNome());

        ProdutoRes produtoRes =
                given()
                        .log().all()
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoCadastro)
                .when()
                        .post("/produtos")
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);

        ProdutoRes produtoNomeRepetidoRes =
                given()
                        .log().all()
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoNomeRepetido)
                .when()
                        .post("/produtos")
                .then()
                        .statusCode(400).extract().as(ProdutoRes.class);


        Assertions.assertEquals("Já existe produto com esse nome", produtoNomeRepetidoRes.getMessage());


                given()
                        .pathParam("id", produtoRes.get_id())
                        .header("Authorization", tokenAdm)
                .when()
                        .delete("/produtos/{id}")
                .then().log().all()
                        .statusCode(200);
    }

    @Test
    public void testBuscarProdutoPorIdValido() {
        ProdutoReq produtoCadastro = produto(); // Para adicionar um produto


        ProdutoRes produtoRes =
                given()

                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoCadastro)
                .when()
                        .post("/produtos")
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);


       ProdutoRes buscarPorId =
               given()
                        .pathParam("id", produtoRes.get_id())
               .when()

                        .get("/produtos/{id}")

               .then()

                        .statusCode(200).extract().as(ProdutoRes.class);

        Assertions.assertNotNull(buscarPorId.get_id());


                given()
                        .pathParam("id", produtoRes.get_id())
                        .header("Authorization", tokenAdm)
                .when()
                        .delete("/produtos/{id}")
                .then().log().all()
                        .statusCode(200);
    }

    @Test
    public void testBuscarProdutoPorIdInvalido() {
        String idProduto = "testestetstetstets";


        ProdutoRes produtoIdInvalidores =
        given()
                .pathParam("id", idProduto)
        .when()
                .get("/produtos/{id}")
        .then()
                .statusCode(400).extract().as(ProdutoRes.class);


        Assertions.assertEquals("Produto não encontrado", produtoIdInvalidores.getMessage());
    }

    @Test
    public void testEditarProdutoComSucesso() {
        ProdutoReq produtoCadastro = produto();
        ProdutoReq produtoEditado = produto();

        ProdutoRes produtoRes =
                given()
                        .log().all()
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoCadastro)
                .when()
                        .post("/produtos")
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);


        ProdutoRes produtoEditadoRes =
                given()
                        .pathParam("id", produtoRes.get_id())
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoEditado)
                .when()
                        .put("/produtos/{id}")
                .then()
                        .statusCode(200).extract().as(ProdutoRes.class);

        Assertions.assertEquals("Registro alterado com sucesso", produtoEditadoRes.getMessage());

                given()
                        .pathParam("id", produtoRes.get_id())
                        .header("Authorization", tokenAdm)
                .when()
                        .delete("/produtos/{id}")
                .then().log().all()
                        .statusCode(200);

    }

    @Test
    public void testEditarProdutoComUmNomeJaUsado() {
        ProdutoReq produtoCadastro = produto();
        ProdutoReq produtoPraEditar = produto();

        ProdutoReq produtoNomeRepetido = produto();
        produtoNomeRepetido.setNome(produtoCadastro.getNome());

                given()
                        .log().all()
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoCadastro)
                .when()
                        .post("/produtos")
                .then()
                        .statusCode(201);

        ProdutoRes produtoRes =
                given()
                        .log().all()
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoPraEditar)
                .when()
                        .post("/produtos")
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);


        ProdutoRes produtoEditadoRes =
                given()
                        .log().all()

                        .pathParam("id", produtoRes.get_id())
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoNomeRepetido)
                .when()
                        .put("/produtos/{id}")
                .then()
                        .statusCode(400).extract().as(ProdutoRes.class);
        Assertions.assertEquals("Já existe produto com esse nome", produtoEditadoRes.getMessage());


                given()
                        .pathParam("id", produtoRes.get_id())
                        .header("Authorization", tokenAdm)
                .when()
                        .delete("/produtos/{id}")
                .then().log().all()
                        .statusCode(200);
    }

    @Test
    public void testTentarEditarProdutoNaoSendoUsuarioAdministrador() {

        ProdutoReq produtoCadastro = produto();
        ProdutoReq produtoEditado = produto();

        ProdutoRes produtoRes =
                given()
                        .log().all()
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoCadastro)
                .when()
                        .post("/produtos")
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);


        ProdutoRes produtoEditadoRes =
                given()
                        .pathParam("id", produtoRes.get_id())
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenNaoAdm)
                        .body(produtoEditado)
                .when()
                        .put("/produtos/{id}")
                .then()
                        .statusCode(403).extract().as(ProdutoRes.class);

        Assertions.assertEquals("Rota exclusiva para administradores", produtoEditadoRes.getMessage());


                given()
                        .pathParam("id", produtoRes.get_id())
                        .header("Authorization", tokenAdm)
                .when()
                        .delete("/produtos/{id}")
                .then().log().all()
                        .statusCode(200);
    }

    @Test
    public void testExcluirProdutoComSucesso() {
        ProdutoReq produtoCadastro = produto();

        ProdutoRes produtoRes =
                given()
                        .log().all()
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoCadastro)
                .when()
                        .post("/produtos")
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);

        ProdutoRes produtoExcluirRes =
                given()
                        .pathParam("id", produtoRes.get_id())
                        .header("Authorization", tokenAdm)
                .when()
                        .delete("/produtos/{id}")
                .then()
                .statusCode(200).extract().as(ProdutoRes.class);

        Assertions.assertEquals("Registro excluído com sucesso", produtoExcluirRes.getMessage());

    }

    @Test
    public void testTentarExcluirProdutoNaoSendoUsuarioAdministrador() {

        ProdutoReq produtoCadastro = produto();

        ProdutoRes produtoRes =
                given()
                        .log().all()
                        .contentType(ContentType.JSON)
                        .header("Authorization", tokenAdm)
                        .body(produtoCadastro)
                .when()
                        .post("/produtos")
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);

        ProdutoRes produtoExcluirRes =
                given()
                        .pathParam("id", produtoRes.get_id())
                        .header("Authorization", tokenNaoAdm)
                .when()
                        .delete("/produtos/{id}")
                .then()
                        .statusCode(403).extract().as(ProdutoRes.class);

        Assertions.assertEquals("Rota exclusiva para administradores", produtoExcluirRes.getMessage());

                given()
                        .pathParam("id", produtoRes.get_id())
                        .header("Authorization", tokenAdm)
                .when()
                        .delete("/produtos/{id}")
                .then().log().all()
                        .statusCode(200);
    }

    private ProdutoReq produto() {
        ProdutoReq produto = new ProdutoReq();
        produto.setNome(faker.commerce().productName());
        produto.setPreco(faker.number().positive());
        produto.setDescricao(faker.lorem().sentence());
        produto.setQuantidade(faker.number().positive());
        return produto;
    }
}

