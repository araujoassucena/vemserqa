package com.vemser.produto;

public class ProdutoRes extends ProdutoReq {
    private String message;
    private  String _id;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "ProdutoRes{" +
                "message='" + message + '\'' +
                ", _id='" + _id + '\'' +
                '}';
    }



}
