package com.vemser.tests;

import io.restassured.http.ContentType;
import org.junit.*;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;


public class ProdutoTest {

    String urlLogin = "http://localhost/login";
    String bodyLoginADM = """
            {
              "email": "assucena@gmail.com",
              "password": "1233"
            }
            """;
    String bodyLoginNaoAdm = """
            {
              "email": "teste00@gmail.com",
              "password": "1233"
            }""";
    String tokenAdm;
    String tokenNaoAdm;

    @Before
    public void setup() {
        baseURI = "http://localhost";
        port = 3000;

        tokenAdm = given()
                .body(bodyLoginADM)
                    .contentType(ContentType.JSON)
                .when()
                    .post(urlLogin)
                .then()
                    .extract()
                    .path("authorization");

        tokenNaoAdm = given()
                .body(bodyLoginNaoAdm)
                    .contentType(ContentType.JSON)
                .when()
                    .post(urlLogin)
                .then()
                    .extract()
                    .path("authorization");
    }

    @Test
    public void testCadastrarProdutoComSucesso() {

        given()
                .contentType(ContentType.JSON)
                .header("authorization", tokenAdm)
                .body(
                        """
                                {
                                  "nome": " carro de brinquedo teste 3",
                                  "preco": 470,
                                  "descricao": "Mouse",
                                  "quantidade": 381
                                }
                                """)
        .when()
                .post("/produtos")
        .then()
                .statusCode(201)
                .body("message", equalTo("Cadastro realizado com sucesso"));


    }

    @Test
    public void testTentarCadastrarProdutoComNomeJaUtilizado() {

        given()
                .contentType(ContentType.JSON)
                .header("Authorization", tokenAdm)
                .body(
                        """
                                {
                                  "nome": "produto teste 1",
                                  "preco": 470,
                                  "descricao": "TESTE",
                                  "quantidade": 381
                                }
                                """)
        .when()
                .post("/produtos")
        .then()
                .statusCode(400)
                .body("message", equalTo("Já existe produto com esse nome"));

    }

    @Test
    public void testBuscarUsuarioPorIdValido() {
        String idProduto = "ZsDFZhFeQ02SnSpv";

        given()
                .pathParam("id", idProduto)
        .when()
                .get("/produtos/{id}")
        .then()
                .statusCode(200)
                .body("preco", equalTo(22));

    }

    @Test
    public void testBuscarUsuarioPorIdInvalido() {
        String idProduto = "testestetstetstets";

        given()
                .pathParam("id", idProduto)
        .when()
                .get("/produtos/{id}")
        .then()
                .statusCode(400)
                .body("message", equalTo("Produto não encontrado"));

    }

    @Test
    public void testEditarProdutoComSucesso() {
        String idProduto = "ZsDFZhFeQ02SnSpv";

        given()
                .pathParam("id", idProduto)
                .contentType(ContentType.JSON)
                .body("""
                        {
                          "nome": "prduto teste 1 editado",
                          "preco": 470,
                          "descricao": "Mouse editado",
                          "quantidade": 3998
                        }
                        """)
        .when()
                .put("/produtos/{id}")
        .then()
                .statusCode(200)
                .body("message", equalTo("Registro alterado com sucesso"));
    }

    @Test
    public void testEditarProdutoComUmNomeJaUsado() {
        String idProduto = "ZsDFZhFeQ02SnSpv";

        given()
                .pathParam("id", idProduto)
                .contentType(ContentType.JSON)
                .body("""
                        {
                          "nome": "prduto teste 1 editado",
                          "preco": 123,
                          "descricao": "teste",
                          "quantidade": 321
                        }
                        """)
        .when()
                .put("/produtos/{id}")
        .then()
                .statusCode(400)
                .body("message", equalTo("Já existe produto com esse nome"));
    }

    @Test
    public void testTentarEditarProdutoNaoSendoUsuarioAdministrador() {
        String idProduto = "ZsDFZhFeQ02SnSpv";

        given()
                .header("Authorization", tokenNaoAdm)
                .pathParam("id", idProduto)
                .contentType(ContentType.JSON)
                .body("""
                        {
                          "nome": "prduto teste 1 editado",
                          "preco": 123,
                          "descricao": "teste",
                          "quantidade": 321
                        }
                        """)
        .when()
                .put("/produtos/{id}")
        .then()
                .statusCode(403)
                .body("message", equalTo("Rota exclusiva para administradores"));
    }

    @Test
    public void testExcluirProdutoComSucesso() {
        String idProduto = "ZsDFZhFeQ02SnSpv";

        given()
                .pathParam("id", idProduto)
        .when()
                .delete("/produtos/{id}")
        .then()
                .statusCode(200)
                .body("message", equalTo("Registro excluído com sucesso | Nenhum registro excluído"));
    }

    @Test
    public void testTentarExcluirProdutoNaoSendoUsuarioAdministrador() {

        String idProduto = "ZsDFZhFeQ02SnSpv";

        given()
                .pathParam("id", idProduto)
                .contentType(ContentType.JSON)
                .header("Authorization", tokenNaoAdm)
        .when()
                .delete("/produtos/{id}")
        .then()
                .statusCode(403)
                .body("message", equalTo("Rota exclusiva para administradores"));
    }

}

