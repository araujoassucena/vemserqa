CREATE TABLE VEM_SER.ESTUDANTE (
 id_estudante NUMBER,
 nome VARCHAR2(200) NOT NULL,
 data_nascimento DATE NOT NULL,
 nr_matricula CHAR(10) UNIQUE NOT NULL,
 ativo CHAR(1) NOT NULL,
 PRIMARY KEY(id_estudante)
);

CREATE SEQUENCE VEM_SER.SEQ_ESTUDANTE
START WITH 1
INCREMENT BY 1
NOCACHE NOCYCLE;



INSERT INTO  VEM_SER.ESTUDANTE (id_estudante, nome, data_nascimento, nr_matricula, ativo)
 VALUES (1, 'Assucena', TO_DATE('25-05-2000', 'dd-mm-yyyy'), 'MAT12345', 'S');


INSERT INTO  VEM_SER.ESTUDANTE (id_estudante, nome, data_nascimento, nr_matricula, ativo)
VALUES (2, 'Maria', TO_DATE('06-04-1998', 'dd-mm-yyyy'), 'MAT56789', 'N');


INSERT INTO VEM_SER.ESTUDANTE (id_estudante, nome, data_nascimento, nr_matricula, ativo)
VALUES (3, 'Carlos', TO_DATE('04-05-2000', 'dd-mm-yyyy'), 'MAT99999', 'S');


INSERT INTO VEM_SER.ESTUDANTE (id_estudante, nome, data_nascimento, nr_matricula, ativo) 
VALUES (4, 'Ana', TO_DATE('23-06-2002', 'dd-mm-yyyy'), 'MAT77777', 'N');


INSERT INTO VEM_SER.ESTUDANTE (id_estudante, nome, data_nascimento, nr_matricula, ativo)
VALUES (5, 'Pedro', TO_DATE('12-07-1999', 'dd-mm-yyyy'), 'MAT44444', 'S');


INSERT INTO VEM_SER.ESTUDANTE (id_estudante, nome, data_nascimento, nr_matricula, ativo)
VALUES (6, 'Mariana', TO_DATE('09-10-2000', 'dd-mm-yyyy'), 'MAT22222', 'N');


INSERT INTO VEM_SER.ESTUDANTE (id_estudante, nome, data_nascimento, nr_matricula, ativo)
VALUES (7, 'Lucas', TO_DATE('12-03-2000', 'dd-mm-yyyy'), 'MAT88888', 'S');


INSERT INTO VEM_SER.ESTUDANTE (id_estudante, nome, data_nascimento, nr_matricula, ativo)
VALUES (8, 'Beatriz', TO_DATE('15-09-1999', 'dd-mm-yyyy'), 'MAT66666', 'N');


INSERT INTO VEM_SER.ESTUDANTE (id_estudante, nome, data_nascimento, nr_matricula, ativo)
VALUES (9, 'Gabriel', TO_DATE('08-12-2000', 'dd-mm-yyyy'), 'MAT33333', 'S');


INSERT INTO VEM_SER.ESTUDANTE (id_estudante, nome, data_nascimento, nr_matricula, ativo)
VALUES (10, 'Laura', TO_DATE('16-09-1998', 'dd-mm-yyyy'), 'MAT55555', 'N');


SELECT id_estudante, nome FROM ESTUDANTE;
SELECT data_nascimento, nr_matricula, ativo FROM ESTUDANTE; 
SELECT * FROM ESTUDANTE;